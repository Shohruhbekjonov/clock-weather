import 'package:flutter/cupertino.dart';

// ignore: missing_return
String getWeekDay(int day) {
  switch (day) {
    case 1:
      return "Dushanba";
      break;
    case 2:
      return "Seshanba";
      break;
    case 3:
      return "Chorshanba";
      break;
    case 4:
      return "Payshanba";
      break;
    case 5:
      return "Juma";
      break;
    case 6:
      return "Shanba";
      break;
    case 7:
      return "Yakshanba";
      break;
  }
}

String getRealMinute(int minute) {
  if (minute < 10) {
    return "0$minute";
  } else
    return "$minute";
}

// ignore: missing_return
String getMonth(int day) {
  switch (day) {
    case 1:
      return "Yanvar";
      break;
    case 2:
      return "Fevral";
      break;
    case 3:
      return "Mart";
      break;
    case 4:
      return "Aprel";
      break;
    case 5:
      return "May";
      break;
    case 6:
      return "Iyun";
      break;
    case 7:
      return "Iyul";
      break;
    case 8:
      return "Avgust";
      break;
    case 9:
      return "Sentabr";
      break;
    case 10:
      return "Oktabr";
      break;
    case 11:
      return "Noyabr";
      break;
    case 12:
      return "Dekabr";
      break;
  }
}

selectWeatherImage(String weatherCode) {
  switch (weatherCode) {
    case "clear":
      AssetImage('assets/weather/clear.png');
      break;
    case "fog":
      AssetImage('assets/weather/fog.png');
      break;
    case "heavy_rain":
      AssetImage('assets/weather/heavy_rain.png');
      break;
    case "heavy_snow":
      AssetImage('assets/weather/heavy_snow.png');
      break;
    case "heavy_sleet":
      AssetImage('assets/weather/heavy_sleet.png');
      break;
    case "light_rain":
      AssetImage('assets/weather/light_rain.png');
      break;
    case "light_sleet":
      AssetImage('assets/weather/light_sleet.png');
      break;
    case "light_snow":
      AssetImage('assets/weather/light_snow.png');
      break;
    case "mostly_clear":
      AssetImage('assets/weather/mostly_clear.png');
      break;
    case "mostly_cloudy":
      AssetImage('assets/weather/mostly_cloudy.png');
      break;
    case "overcast":
      AssetImage('assets/weather/overcast.png');
      break;
    case "partly_cloudy":
      AssetImage('assets/weather/partly_cloudy.png');
      break;
    case "rain":
      AssetImage('assets/weather/rain.png');
      break;
    case "sleet":
      AssetImage('assets/weather/sleet.png');
      break;
    case "snow":
      AssetImage('assets/weather/snow.png');
      break;
    case "thunderstorm":
      AssetImage('assets/weather/thunderstorm.png');
      break;

    default:
      AssetImage('assets/weather/clear.png');
  }
}