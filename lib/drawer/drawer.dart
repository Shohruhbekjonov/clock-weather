import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lite_rolling_switch/lite_rolling_switch.dart';

import 'drawer_body/drop_down_btn.dart';
import 'drawer_body/switch.dart';

class GetDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var isDark = Theme.of(context).brightness == Brightness.dark;

    return Drawer(
      child: SafeArea(
        child: Container(
          padding: EdgeInsets.all(16),
          child: Column(
            children: [
              DrawerSwitch(),
              SizedBox(height: 16),
              DropDownBtn(),
            ],
          ),
        ),
      ),
    );
  }

  // drawerSwitch(bool isDark) {
  //   LiteRollingSwitch(
  //     value: isDark,
  //     textOff: "night",
  //     textOn: "day",
  //     colorOff: Colors.yellow[600],
  //     colorOn: Colors.blueGrey,
  //     iconOff: Icons.nights_stay,
  //     iconOn: Icons.wb_sunny,
  //     onChanged: (bool position) {
  //       position
  //           ? Get.changeTheme(ThemeData.dark())
  //           : Get.changeTheme(ThemeData.light());
  //       // print("isDay = $position");
  //     },
  //   );
  // }
}
