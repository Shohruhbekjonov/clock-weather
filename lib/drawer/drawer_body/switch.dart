import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lite_rolling_switch/lite_rolling_switch.dart';

class DrawerSwitch extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    var isDark = Theme.of(context).brightness == Brightness.dark;
    print(isDark);
    return LiteRollingSwitch(
      value: isDark,
      textOff: "night",
      textOn: "day",
      colorOff: Colors.yellow[600],
      colorOn: Colors.blueGrey,
      iconOff: Icons.nights_stay,
      iconOn: Icons.wb_sunny,

      onChanged: (bool position) {
        position
            ? Get.changeTheme(ThemeData.dark())
            : Get.changeTheme(ThemeData.light());
        // print("isDay = $position");
      },
    );
  }
}
