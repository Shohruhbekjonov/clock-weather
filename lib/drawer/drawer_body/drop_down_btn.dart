import 'package:clock_app/controllers/api_controller.dart';
import 'package:clock_app/drawer/drawer_body/getRegion.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DropDownBtn extends StatelessWidget {
  ApiController apiController = Get.put(ApiController());
  int _value = 1;
  @override
  Widget build(BuildContext context) {
    return DropdownButton(
      value: _value,
      dropdownColor: Colors.blueGrey,
      // iconDisabledColor: Colors.red,
      // iconEnabledColor: Colors.red,
      // iconSize: 24,
      items: [
        DropdownMenuItem(
          child: Text('Toshkent'),
          value: 1,
        ),
        DropdownMenuItem(
          child: Text('Nukus'),
          value: 2,
        ),
        DropdownMenuItem(
          child: Text('Urganch'),
          value: 3,
        ),
        DropdownMenuItem(
          child: Text('Samarqand'),
          value: 4,
        ),
        DropdownMenuItem(
          child: Text('Jizzax'),
          value: 5,
        ),
        DropdownMenuItem(
          child: Text('Guliston'),
          value: 6,
        ),
        DropdownMenuItem(
          child: Text('Navoiy'),
          value: 7,
        ),
        DropdownMenuItem(
          child: Text('Qarshi'),
          value: 8,
        ),
        DropdownMenuItem(
          child: Text('Termiz'),
          value: 9,
        ),
        DropdownMenuItem(
          child: Text('Buxoro'),
          value: 10,
        ),
        DropdownMenuItem(
          child: Text('Farg`ona'),
          value: 11,
        ),
        DropdownMenuItem(
          child: Text('Andijon'),
          value: 12,
        ),
        DropdownMenuItem(
          child: Text('Namangan'),
          value: 13,
        ),
      ],
      onChanged: (int value) {
            _value = value;
            apiController.pulFromApi(getRegion(_value), getRegionName(_value));
            // region = getRegion(value);
            // regionName = getRegionName(value);
            // pullFromApi(region);
            // apiController.pullFromApi(region);
      },
    );
  }
}
