import 'package:clock_app/controllers/timer_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

// ignore: non_constant_identifier_names
Widget Clock (BuildContext context) {
  TimerController timerController =Get.put(TimerController());
  var isDark = Theme
      .of(context)
      .brightness == Brightness.dark;
  return Container(
    width: double.infinity,
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(24),
      border: Border.all(color: Colors.blue, width: 2),
    ),
    child: Stack(
      overflow: Overflow.visible,
      alignment: AlignmentDirectional.center,
      children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Obx(() => Text(
              timerController.time.value,
              // '19:00',
              style:
              // Theme.of(context).textTheme.bodyText1,
              TextStyle(
                fontSize: 65,
                fontWeight: FontWeight.bold,
                color: isDark ? Colors.white : Colors.blue,
              ),
            )),
            Text(
              timerController.date.value,
              // '26-fevral',
              style:
              // Theme.of(context).textTheme.headline1,
              TextStyle(
                fontSize: 32,
                fontWeight: FontWeight.bold,
                color: isDark ? Colors.white : Colors.blueGrey,
              ),
            ),
            Text(
              timerController.weekday.value,
              // 'juma',
              style:
              // Theme.of(context).textTheme.headline2,
              TextStyle(
                fontSize: 40,
                color: isDark ? Colors.white : Colors.blueGrey,
              ),
            ),
          ],
        ),
        Positioned(
          top: -60,
          child:
          Image.asset(
            "assets/images/gerb.png",
            height: 120,
          ),
        )
      ],
    ),
  );
}
