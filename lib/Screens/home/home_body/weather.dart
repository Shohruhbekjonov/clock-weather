import 'package:clock_app/controllers/api_controller.dart';
import 'package:clock_app/network/providers/my_provider.dart';
import 'package:clock_app/utils/choose_data.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

// ignore: must_be_immutable
class Weather extends StatelessWidget {
  MyProvider _myProvider = new MyProvider();
  ApiController apiController = Get.put(ApiController());
  String _temprature = "...";
  AssetImage _selectWeatherImage;

  @override
  Widget build(BuildContext context) {
    var isDark = Theme.of(context).brightness == Brightness.dark;
    return Container(
      color: Colors.transparent,
      padding: EdgeInsets.only(bottom: 32),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Obx(
            () => Text(
              apiController.tempratura.value,
              style: TextStyle(
                fontSize: 48,
                color: isDark ? Colors.white : Colors.black,
              ),
            ),
          ),
          Obx(
            () => Text(
              apiController.regionName.value,
              style: TextStyle(
                fontSize: 30,
                color: isDark ? Colors.white : Colors.black,
              ),
            ),
          ),
        ],
      ),
    );
  }
// void pullFromApi(String region) {
//   // Timer.periodic(Duration(minutes: 30), (timer) {
//   // setIsLoading(true);
//   _myProvider
//       .getWeatherData(region)
//       .then((value) {
//     print(value.city.title);
//     double temp = double.parse(value.airT.toString());
//         if (temp > 0)
//           _temprature = "+$temp";
//         else
//           _temprature = "$temp";
//
//         print('teeeeeemp' + _temprature);
//         _selectWeatherImage = selectWeatherImage(value.weatherCode);
//   })
//       .catchError((onError) => {
//     print(onError.toString()),
//   });
//       // .whenComplete(() => {setIsLoading(false)});
//   // });
// }

}
