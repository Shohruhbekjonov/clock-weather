import 'package:clock_app/Screens/home/home_body/clock.dart';
import 'package:flutter/material.dart';
import 'home_body/weather.dart';

class GetBodyLandscape extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var isDark = Theme.of(context).brightness == Brightness.dark;
    return Container(
      padding: EdgeInsets.symmetric(vertical: 40, horizontal: 32),
      decoration: BoxDecoration(
        image: DecorationImage(
          image: isDark
              ? AssetImage('assets/images/background_light.png')
              : AssetImage('assets/images/background.png'),
          fit: BoxFit.cover,
        ),
      ),
      child: Row(
        children: [
          Expanded(
            flex: 5,
            child: Clock(context),
          ),
          SizedBox(width: 16),
          Expanded(
            flex: 3,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  width: 110,
                  height: 110,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/weather/heavy_sleet.png'),
                    ),
                  ),
                ),
                Weather(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
