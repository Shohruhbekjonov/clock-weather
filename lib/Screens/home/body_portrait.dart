import 'package:clock_app/controllers/api_controller.dart';
import 'package:clock_app/Screens/home/home_body/clock.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'home_body/weather.dart';

class GetBodyPortrait extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ApiController apiController = Get.put(ApiController());
    var isDark = Theme.of(context).brightness == Brightness.dark;

    return Container(
      padding: EdgeInsets.fromLTRB(24, 70, 24, 40),
      decoration: BoxDecoration(
        image: DecorationImage(
          image: isDark
              ? AssetImage('assets/images/background_light.png')
              : AssetImage('assets/images/background.png'),
          fit: BoxFit.cover,
        ),
      ),
      child: Column(
        children: [
          Expanded(
            flex: 5,
            child: Clock(context),
          ),
          SizedBox(height: 16),
          Expanded(
            flex: 3,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Obx(() => Container(
                  width: 110,
                  height: 110,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/weather/heavy_sleet.png'),
                    ),
                  ),
                ),
                ),
                Weather(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
