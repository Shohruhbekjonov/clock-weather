import 'package:clock_app/controllers/api_controller.dart';
import 'package:clock_app/drawer/drawer.dart';
import 'package:clock_app/Screens/home/body_landscape.dart';
import 'package:clock_app/Screens/home/body_portrait.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';


class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final _scaffoldkey = new GlobalKey<ScaffoldState>();
ApiController apiController = Get.put(ApiController());

  @override
  void initState() {
    apiController.pulFromApi("tashkent", "Toshkent");
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        appBar: AppBar(
          toolbarHeight: 0,
        ),
        key: _scaffoldkey,
        endDrawer: GetDrawer(),
        body: OrientationBuilder(builder: (context, orientation) {
          if (orientation == Orientation.portrait) {
            return GetBodyPortrait();
          } else {
            return GetBodyLandscape();
          }
        }),
        floatingActionButton: FloatActBtn(),
      ),
    );
  }

  Widget FloatActBtn() {
    return FloatingActionButton(
      onPressed: () => _scaffoldkey.currentState.openEndDrawer(),
      tooltip: 'Increment',
      child: Icon(Icons.settings_sharp),
    );
  }
  }

  //
// class  extends StatelessWidget {
//   final _scaffoldkey = new GlobalKey<ScaffoldState>();
//  
//   @override
//   Widget build(BuildContext context) {
//     return 
// }
