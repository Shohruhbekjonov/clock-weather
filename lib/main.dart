import 'package:clock_app/Screens/home_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

void main() {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent
  ));

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(

      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: Colors.lightBlue[800],
        accentColor: Colors.cyan,
        scaffoldBackgroundColor: Colors.white,

        fontFamily: 'Georgia',

        textTheme: TextTheme(
          bodyText1: TextStyle(fontSize: 65, fontWeight: FontWeight.bold, color: Colors.blue),
          headline1: TextStyle(fontSize: 32, fontWeight: FontWeight.bold, color: Colors.blueGrey),
          headline2: TextStyle(fontSize: 40, color: Colors.blueGrey),
        ),
      ),
      darkTheme:ThemeData(
        brightness: Brightness.dark,
        primaryColor: Colors.lightBlue[800],
        accentColor: Colors.cyan,
        scaffoldBackgroundColor: Color.fromARGB(100, 0, 26, 72),
        fontFamily: 'Georgia',


        textTheme: TextTheme(
          bodyText1: TextStyle(fontSize: 65, fontWeight: FontWeight.bold, color: Colors.blue),
          headline1: TextStyle(fontSize: 32, fontWeight: FontWeight.bold, color: Colors.white),
          headline2: TextStyle(fontSize: 40, color: Colors.white),
        ),
      ),
      themeMode: ThemeMode.light,

      home: HomeScreen()
    );
  }
}