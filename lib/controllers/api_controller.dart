import 'package:clock_app/network/providers/my_provider.dart';


import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:clock_app/utils/choose_data.dart';


class ApiController extends GetxController {
  MyProvider _myProvider = new MyProvider();

  RxString tempratura = "...".obs;
  RxString regionName = "...".obs;
  RxString selectWetherImage = "...".obs;


  @override
  void onInit() {

  }

  void pulFromApi(String region, String regionName) {
    _myProvider.getWeatherData(region).then((value) {
      print(value.city.title);
      double temp = double.parse(value.airT.toString());

      if (temp > 0)
      this.tempratura.value = "+$temp";
      else
      this.tempratura.value = "$temp";

      this.regionName.value = regionName;
      this.selectWetherImage = selectWeatherImage(value.weatherCode);
      })
        .catchError((onError) =>
    {
      print(onError.toString()),
    });
    // .whenComplete(() => {setIsLoading(false)});

  }
}