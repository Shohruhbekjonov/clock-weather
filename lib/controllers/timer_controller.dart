import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ntp/ntp.dart';
import 'package:clock_app/utils/choose_data.dart';

class TimerController extends GetxController {
  RxString time = "".obs;
  RxString date = "...".obs;
  RxString weekday = "...".obs;
  Timer timer;

  DateTime now;

  @override
  // ignore: must_call_super
  void onInit() {
    super.onInit();

    this.now = DateTime.now();
    this.setTime();

    getTime();
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  void getTime() {
    this.timer = Timer.periodic(Duration(seconds: 1), (timer) {
      this.now = this.now.add(Duration(seconds: 1));
      this.setTime();
    });

    // Duration limit = Duration(seconds: 1);
    // this.now = this.now.add(limit);
    // this.setTime();
    // Future.delayed(limit, () => this.getTime());
  }

  void setTime() {
    time.value = "${this.now.hour}:${getRealMinute(this.now.minute)}";
    date.value = "${this.now.day} - ${getMonth(this.now.month)}";
    weekday.value = getWeekDay(this.now.weekday);
    // _isDayMode = (this.now.hour > 7 && this.now.hour < 19) ? true : false;

    print(this.time.value);
  }
}
